#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define MAX_INTERVAL 25

int main(int argc, char** argv)
{
 	if (argc < 2) 
  	{
    	fprintf(stderr, "Incorrect number of arguments; usage: presblock [pid]\n");
    	exit(EXIT_FAILURE);
  	}

	// Convert text pid to int
  	pid_t proc_pid = atoi(argv[1]);

  	// Check if pid is a sane value
  	if (proc_pid > 0) 
  	{
  		FILE *test = fopen("presblockLog.txt", "w");
  		
  		fprintf(test, "Presblock entered ..\n");
  		
    	for(;;) 
    	{
    		//This is done so when it's parent is killed by shell program, the for loop does not continue 
    		//on. This returns -1 when the passed pid is non-existent.
      		if(kill(proc_pid, 0) == -1)//does not exist
      		{
      			fprintf(test, "Process with pid %d does not exist currently.\n", proc_pid);
     	  		//printf("Process with pid %d does not exist currently.\n", proc_pid);
     	  		break;
      		}else
      		{   	
		  		// AGENT Smith:
		  		
		  		// rand() is the worst PRNG ever; FFS use something else!
		  		int delay = rand() % MAX_INTERVAL;

		  		// Delay and send signal
		  		//debugging purposes, to see of the contents arrived through the pipes. SUCCESS
        	
        	
        		fprintf(test, "Delaying for %d seconds...\n", delay);
        	

		  		//printf("Delaying for %d seconds...\n", delay);
		  		sleep(delay);
		  		
		  		if(kill(proc_pid, SIGALRM) == -1)
		  		{
		  			fprintf(test, "Error sending signal\n");
		  		}
      		}
    	}
    	
    	        	fclose(test);
  	} else 
  	{
    	fprintf(stderr, "Invalid pid! Exiting...\n");
    	exit(EXIT_FAILURE);
  	}

  	// Unreachable code
  	exit(EXIT_SUCCESS);
}

