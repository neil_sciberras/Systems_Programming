//BUG: Setting the buffer size too small, will end up being a problem resizing it to a more suitable size.

#include <ncurses.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/shm.h> //for shared memory segment
#include <signal.h>
#include <time.h>

#define NUM_OF_TOKENS 64
#define DELIMITERS " \t\r\n\a"

//Since the buffer should be a 2d array, but the environment variables are just normal variables, 
//BUF_CHARS is the number of characters per line, which ant be changed, 
//and then in the environemtn variable we will save the number of lines, which could also be 
//change by the user. 
#define BUF_CHARS 256 

WINDOW * createNewWindow(int h, int w, int startY, int startX);
void getLine(int * bufPointer1, int * bufPointer2);
char ** splitLine(char * line);
int launch(char ** args);

int writeToBuffer(char * line);
int printBuffer(int startLine, int lines);
void printOutputW(char * line);

void errorPrint(int errorNum);

int sh_cd(char ** args);
int shdir(char ** args);
int print(char ** args);
int printvar(char ** args);
int set(char ** args);
int smove(char ** args);
int sh_exit(char ** args);


//list of built in commands
char * builtinStr[] = {
        "chdir",
        "exit",
        "shdir",
        "print",
        "printvar",
        "set", 
        "smove"
};

//their corresponding functions
//array of functions pointers that take array of strings and return an int
int (*builtinFunc[]) (char **) = {
        &sh_cd,
        &sh_exit,
        &shdir,
        &print,
        &printvar,
        &set, 
        &smove
};

int sh_num_builtins(void);

int execute(char ** args);

int errCount = 0;

//globally decalring windows so the pointer could be used from anywhere
WINDOW *dateWin;
int dateH, dateW;

WINDOW *alarmWin;
int alarmH, alarmW;

WINDOW *borderWin;
WINDOW *colourWin;

WINDOW *outputWin;
int outputCounter = 0; //global counter of the lines in the output window
int outputH, outputW;

WINDOW *promptWin;
int promptCounter = 1; //global counter of the lines in the prompt window
int promptCol = 0; //counts where the cursor should accorindg to the columns
int promptH, promptW;

char *buffer[256];
int bufferCount = 0;

int lastLine = -1;

int shellPID; //global variable, so the child will inherit the value of the 

char recPID[20]; //this if not here, should go to the first child of the shell (the root parent sort of)

char line[100]; //storing the command and the args (whole line)

struct sigaction sa;

volatile sig_atomic_t interruptRec = 0;
void sig_handler(int signo);
//void sig_alarm_handler(int signum, siginfo_t *info, void *ptr);
//void catch_sigalrm();




int main(void)
{
			initscr();
			start_color();
			
			int stdscrHeight;
			int stdscrWidth;

			//retrieving the size of the screen and storing the width and height
			getmaxyx(stdscr, stdscrHeight, stdscrWidth);

			//refreshing so as the changes made regarding the colour appear
			refresh();


			//initialising the window of the date and time
			dateWin = createNewWindow(stdscrHeight/4, stdscrWidth/2, 0, 0);
			//initiaising the window of the alarm panel
			alarmWin = createNewWindow(stdscrHeight/4, (7*stdscrWidth/16), 0, stdscrWidth/2);
			//initiaising the window of the colour panel
			borderWin = createNewWindow(stdscrHeight/4, stdscrWidth/16, 0, 15*stdscrWidth/16);
			colourWin = newwin((stdscrHeight/4)-2, (stdscrWidth/16)-2, 1, (15*stdscrWidth/16)+1);
			//initialising the window of the output
			outputWin = createNewWindow(stdscrHeight/2, stdscrWidth, stdscrHeight/4, 0);
			//initialising the prompt window
			promptWin = createNewWindow(stdscrHeight/4, stdscrWidth, (3 * stdscrHeight)/4, 0);

			//*****************************************************************************************************************
			
			scrollok(outputWin,TRUE);
			scrollok(alarmWin,TRUE);

			getmaxyx(promptWin, promptH, promptW);
			getmaxyx(outputWin, outputH, outputW);
			getmaxyx(alarmWin, alarmH, alarmW);	
			getmaxyx(dateWin, dateH, dateW);
	
	int pipe1[2]; //the parents writes the pid of the child, then the child reads from the other end of the pipe, and
	//sends it to its child. 

	pid_t pid;
	
	/* Create the pipe. */
  	if (pipe (pipe1))
    {
      //fprintf (stderr, "Pipe failed.\n");
      return EXIT_FAILURE;
    }
	
	//used to fork the process that will catch the signals
	pid = fork();
	
	//parent process
    if(pid > 0)
    {
    	
    	pid_t time_pid;
    	
    	int pipet[2];
    	
    	/* Create the pipe. */
	  	if (pipe (pipet))
		{
		  //fprintf (stderr, "Pipe failed.\n");
		  return EXIT_FAILURE;
		}
    	
    	time_pid = fork();
    	
    	if(time_pid > 0)
    	{
    		//parent
    		
    		
    		//sleep(1);
    		
    		
			int segmentSize =  ((alarmH-2)*(alarmW-2)); //the size of the shared memory segment should be the size in 
			//characters of the panel on the screen. But since we have the border taking 2 lines from each the width and 
			//height, we reduce 2 from each.
			
			int segmentSize1 =  ((dateH-2)*(dateW-2));
			
			
			//PIPE WRITING
			char pidStr[20];
			char sizeStr[20];
			sprintf(pidStr, "%d", pid); //converting the integer to string 
			sprintf(sizeStr, "%d", segmentSize); //converting the integer to string 
			
			//this pipe is going to be used to send the PID of the child of this fork. The child of this fork is the 
			//parent of the process that will be generating the signals. This parent we're in right now has the PID 
			//of the child (parent of the process generating the signals). 
			//So we're using a pipe between this parent and it's child to send the PID. Then we're using another pipe 
			//fromt the child to the other child (the process generating the signals).
			
			close (pipe1[0]); //closing the reading end, because we need to write the PID for the child to read it. 
			//write(pipe1[1], pidStr, 20);
			write(pipe1[1], sizeStr, 20);
			//PIPE WRITING
			
			
			
			//ALARM PANEL MEMORY SEGMENT
			//variables needed for the shared memory segement. Here we're attaching to it and reading from it. 
			int shmid;
			key_t key;
			char *shm, *s;
		
		
			char readLine[200];
		
			/*
			 * We need to get the segment named
			 * "0x1235", created by the child.
			 */
			key = 0x1235;
		
			/*
			 * Locate the segment.
			   We're doing a while loop that loops until a memory segment with the given key is found. 
			   It may bs, since the parent and child are running concurrently, that at a given point 
			   one executes faster than the other. This will lead to the parent (this process) trying to
			   get the segment that has not been yet created. But we know that it will surely be created by,
			   the other process (child). So we do the while loop so it keeps on looping until the child 
			   has finally created the shared memroy segment and we could get it.  
			   
			   Else, to avoid doing the while loop, which may create an infinite loop if something goes wrong,
			   what we could do is make the parent process sleep for few seconds before it begins, and then try to
			   get the memory segment, so as the child will have created the segment already.
			 
			 */		 
			while((shmid = shmget(key, segmentSize, 0666)) < 0)
			{
				continue;
			}
		
		
			/*
			 * Now we attach the segment to our data space.
			 */
			if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) 
			{
				errorPrint(errno);
				exit(1);
			}
			
			int *segBuf = (int*) shm; //this is a buffer treated as a list of pointers to integers.
		
			//strncpy(readLine, shm, 200); //the third argument is the numbe rof bytes to read from the source to dest. 
			//This amount should be able to fit in the destination. 
		
			
			//ALARM PANEL MEMORY SEGMENT
		
			
			
			//TIME PANEL MEMORY SEGMENT
			//variables needed for the shared memory segement. Here we're attaching to it and reading from it. 
			int shmid1;
			key_t key1;
			char *shm1; 
			char *s1;
			
		
		
			char readLine1[200];
			
			char sizetStr[20];
			sprintf(sizetStr, "%d", segmentSize1);
			
			close (pipet[0]); //closing the reading end, because we need to write the PID for the child to read it. 
			write(pipet[1], sizetStr, 20);
		
			/*
			 * We need to get the segment named
			 * "0x1235", created by the child.
			 */
			key1 = 0x1231;
		
			/*
			 * Locate the segment.
			   We're doing a while loop that loops until a memory segment with the given key is found. 
			   It may bs, since the parent and child are running concurrently, that at a given point 
			   one executes faster than the other. This will lead to the parent (this process) trying to
			   get the segment that has not been yet created. But we know that it will surely be created by,
			   the other process (child). So we do the while loop so it keeps on looping until the child 
			   has finally created the shared memroy segment and we could get it.  
			   
			   Else, to avoid doing the while loop, which may create an infinite loop if something goes wrong,
			   what we could do is make the parent process sleep for few seconds before it begins, and then try to
			   get the memory segment, so as the child will have created the segment already.
			 
			 */		 
			while((shmid1 = shmget(key1, segmentSize1, 0666)) < 0)
			{
				continue;
			}
		
			
		
			/*
			 * Now we attach the segment to our data space.
			 */
			if ((shm1 = shmat(shmid1, NULL, 0)) == (char *) -1) 
			{
				errorPrint(errno);
				exit(1);
			}
			
			int *seg1Buf = (int*) shm1; //this is a buffer treated as a list of pointers to integers.
			
			//TIME PANEL MEMORY SEGMENT 
			
		

		

			//char line[100];
			char **arguments;
			int status;

			char promptString[50]; //used to temporarily store the value of the environment variable 'prompt'
			//and then use it to be displayed before every command. 
		
			setenv("prompt", "OK", 1); //setting the default value of 'prompt'
	
			//This is the default amount of lines that can be stored in the buffer.
			//To actually be used, this has to be converted to integer first. 
			setenv("buffer", "80", 1); 
			
			//default value of refresh is 1 second
			setenv("refresh", "1", 1);

			printOutputW("\n");
			outputCounter--; //Since the printOutputW increments the counter, but the newline char is not an actual output, we 
			//decrement it again.
		   	 
			//ALLOCATING THE DEFAULT AMOUNT OF MEMORY TO THE BUFFER (80 LINES 256 CHARACTERS EACH)
		
			//rows
			int r = atoi(getenv("buffer"));
		
			if(r != 0)
			{
				//allocating enough memory to store BUF_CHARS to every row/line
				for (int i=0; i<r; i++)
				{
					 buffer[i] = (char *)malloc(BUF_CHARS * sizeof(char));
				}
		
		
			}
				 
			//ALLOCATING THE DEFAULT AMOUNT OF MEMORY TO THE BUFFER (80 LINES 256 CHARACTERS EACH)
		

			do{
			
				
				
				if(promptCounter >= promptH-1)
				{
				    //when the prompt window is full, re-create it, set the counter back to 1, and move the cursor to the
				    //beginning of the new window again.
				    promptWin = createNewWindow(stdscrHeight/4, stdscrWidth, (3 * stdscrHeight)/4, 0);
				    promptCounter = 1;
				    wmove(promptWin, promptCounter, 1);

				} else
				{
				    wmove(promptWin, promptCounter, 1);
				}

				wprintw(promptWin, "%s> ", getenv("prompt"));
				
				wrefresh(promptWin);//refreshing the window itself

				//we are getting a line in the window promptWin and storing it in line
				//wgetstr(promptWin, line);
				seg1Buf[2] = atoi(getenv("refresh"));
				getLine(seg1Buf, segBuf);

				arguments = splitLine(line);

				status = execute(arguments);

				//writing the refresh value in the memory segment after every command
				strcpy(shm1, getenv("refresh"));
				
				free(arguments);

			}while(status);
			//status will be zero (and hence stops the loop) for example when exit is called and status is set to 0.


			wrefresh(promptWin);//refreshing the window itself


			delwin(dateWin);
			delwin(alarmWin);
			delwin(outputWin);
			delwin(promptWin);

			//ending ncurses mode
			endwin();


            if(shmctl(shmid1, IPC_RMID, NULL) == -1)
            {
                errorPrint(errno);
                exit(1);
            }

            //removes the shared memory identifier specified by shmid from the system
            //and destroys the shared memory segment and shmid_ds (3rd argument) associated with it.
            //on error it will return -1
            if(shmctl(shmid, IPC_RMID, NULL) == -1)
            {
                errorPrint(errno);
                exit(1);
            }
		    
		    
		
			kill(time_pid, SIGTERM); //this will kill the child process 
			kill(0, SIGTERM); //this will kill all the process in the process group of this parent.
			
			exit(0);
			
		}else if(time_pid == 0)
			{	
				
				
				//child updating the shared mem segment with the time
				
				char rec_sizet[20];
        
				close(pipet[1]); //close write end because we're reading.
				read(pipet[0], rec_sizet, 20);
				
				//Variables need for the shared memory segment. This process is the producer (writes to the memory segment).
				char c1;
				int shmid1;
				key_t key1;
				char *shm1 = (char *) malloc(200); 
				char *s1;
				size_t size1 = atoi(rec_sizet);
				
		
				key1 = 0x1231; //the 'logical' address of the memory for the other process to be able to attach to it.
		
				/*
				 * Create the segment.
				 */
				if ((shmid1 = shmget(key1, size1, IPC_CREAT | 0666)) < 0)
				{
					writeToBuffer(strerror(errno));
					printBuffer(outputCounter, 1);
					
					exit(1);
				}
		
				/*
				 * Now we attach the segment to our data space.
				 */
				if ((shm1 = shmat(shmid1, NULL, 0)) == (char *) -1) {
					errorPrint(errno);
				
					exit(1);
				}
				
				sleep(1);
				int *BUF = (int*) shm1;
				
				
				//keeps on looping until the parent exits.
				while(kill(getppid(), 0) != -1)
				{
					sleep(BUF[2]);

					setenv("TZ", "Europe/Malta", 1);
						
			        time_t malta;
				        
			        malta = time(NULL);

					char timeRes1[200];
				
					strcpy(timeRes1, "Malta:       ");
					strcat(timeRes1, asctime(localtime(&malta)));
					strcat(timeRes1, "  ");

				
					setenv("TZ", "America/New_York", 1);
				
					time_t us;

			        us = time(NULL);

					strcat(timeRes1, "White House: ");
					strcat(timeRes1, asctime(localtime(&us)));
				
					strcat(timeRes1, "  ");
				
					setenv("TZ", "Asia/Tokyo", 1);
				
					time_t japan;

			        japan = time(NULL);

					strcat(timeRes1, "Japan:       ");
					strcat(timeRes1, asctime(localtime(&japan)));
					//strcpy(shm1, timeRes1);
						
					
					mvwprintw(dateWin, 1, 1, "\n  %s", timeRes1);
					box(dateWin, 0, 0);
					wrefresh(dateWin);
					wmove(promptWin, BUF[1], BUF[0]);	
					wrefresh(promptWin);
						
				}
					
				endwin();
				
                shmdt(shm1); //this only detaches from the memory segment, but does not delete the memory segment
                
                exit(0);

			}else
			{
				errorPrint(errno);
				//error
			}
			
			kill(0, SIGTERM);
    	

    }else if(pid == 0) 
    {
        //child process
    
        //we recevie the pid of this child by reading from pipe1. 
        //First we need to close the writing end of the pipe. 
        
        char recSize[20]; //this will be inherited by the children this process will create.
        
        close(pipe1[1]);
        //read(pipe1[0], recPID, 20);
        read(pipe1[0], recSize, 20);
        close(pipe1[0]);
        
        pid_t pid2;
        
        pid2 = fork();  
        
        if(pid2 > 0)
        {
        
            //ALARM PANEL MEMORY SEGMENT
            //Variables need for the shared memory segment. This process is the producer (writes to the memory segment).
            char c;
            int shmid;
            key_t key;
            char *shm = (char *) malloc(200);
            char *s;
            int size = atoi(recSize);

            key = 0x1235; //the 'logical' address of the memory for the other process to be able to attach to it.

            /*
             * Create the segment.
             */
            if ((shmid = shmget(key, size, IPC_CREAT | 0666)) < 0)
            {
                errorPrint(errno);

                exit(1);
            }

            /*
             * Now we attach the segment to our data space.
             */
            if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) {
                errorPrint(errno);

                exit(1);
            }
            
            int alarmCounter = 0;
            
            int *BUF2 = (int*) shm;


            //ALARM PANEL MEMORY SEGMENT


        	//parent process
            char sigsToPID[20];
            sprintf(sigsToPID, "%d", pid2);

			
			if(signal(SIGALRM, sig_handler) == SIG_ERR)
		    {
		    	writeToBuffer("Error receving signal..\n");
				printBuffer(outputCounter, 1);
				
		    }
		    
		    
		    time_t old = (time_t) 0;
		    time_t new = (time_t) 0;
		    double diff = 0;
		    
		    //these will be used to convert the times into strings
		    struct tm *oldInfo;
		    struct tm *newInfo;
		    
		    char timeStr[30];
			
			while(1)
			{
				//the interrupt handler (the function) sets this variable to 1 from 0, so thats how we 
				//are detecting whether there is a signal or no. 
		        if(interruptRec == 1)
		        {	
		        	
		        	//necessary for the conversion of time to string.
		        	time(&new);
		        	newInfo = localtime( &new );
		        	
		        	if(old != (time_t) 0)
		        	{
				    	if((diff = difftime(new, old)) < 0)
				    	{
				    		diff = (double) (60-abs(diff));
				    	}
		        	}
		        	
		        	if((int) diff > 20)
		        	{
		        		refresh();
						start_color();
						init_pair(1, COLOR_BLACK, COLOR_BLUE);
						wbkgd(colourWin,COLOR_PAIR(1));
						wrefresh(colourWin);
						
		        	}else if(((int) diff >= 16) && ((int) diff <= 20))
		        	{
		        		refresh();
						start_color();
						init_pair(1, COLOR_BLACK, COLOR_GREEN);
						wbkgd(colourWin,COLOR_PAIR(1));
						wrefresh(colourWin);
		        	}else if(((int) diff >= 11) && ((int) diff <= 15))
		        	{ //ORANGE
		        		
		        		init_color(COLOR_RED, 255, 165, 0);
		        		
		        		refresh();
						start_color();
						init_pair(1, COLOR_BLACK, COLOR_RED);
						wbkgd(colourWin,COLOR_PAIR(1));
						wrefresh(colourWin);
		        	}else if(((int) diff >= 5) && ((int) diff <= 10))
		        	{
		        		init_color(COLOR_RED, 1000, 0, 0);
		        		
		        		refresh();
						start_color();
						init_pair(1, COLOR_BLACK, COLOR_RED);
						wbkgd(colourWin,COLOR_PAIR(1));
						wrefresh(colourWin);		        		
		        	}else if((int) diff < 5)
		        	{
		        		refresh();
						start_color();
						init_pair(1, COLOR_BLACK, COLOR_WHITE);
						wbkgd(colourWin,COLOR_PAIR(1));
						wrefresh(colourWin);
		        	}
		        	
		        	strftime(timeStr,30,"%H:%M:%S", newInfo);
		        	
		        	if(alarmCounter >= alarmH-2)
		        	{
		        		alarmCounter = 0; //reset to zero so this if statement will be useful for the next time 
		        		//the alarm counter reaches the specified value in the if statement.
		        		
		        		alarmWin = createNewWindow(stdscrHeight/4, (7*stdscrWidth/16), 0, stdscrWidth/2);
		        		
		        		
		        	}
		        	
		        	mvwprintw(alarmWin, alarmCounter+1, 1, "[%s] Alarm Received", timeStr);
					box(alarmWin, 0, 0);
					wrefresh(alarmWin);
					wmove(promptWin, BUF2[1], BUF2[0]);	
					wrefresh(promptWin);
		        	
		        	
		        	//setting back 0, so as an incoming signal can put it back to 1 and the signal can be detected
		        	interruptRec = 0; 
		        	
		        	alarmCounter++;
		        	
		        	old = new;
		        }
            }
            
            endwin();
            
            shmdt(shm); //this only detaches from the memory segment, but does not delete the memory segment
        
        	free(shm);
        	
        	exit(0);
        	
        	
        }else if(pid2 < 0)
        {
        	//error
        	writeToBuffer(strerror(errno));
			printBuffer(outputCounter, 1);
        	
        }else
        {       
        
        	//2nd child process
        	//The goal of this child is to generate signals and send them to the child of the shell, which will then 
        	//communicate with the shell itself and display the accoridng messages on the alarm panel. 
        	
        	char stringPID[20];
        	
        	
        	int parPID = getppid();
        	
        	sprintf(stringPID, "%d", parPID);
        	
        	char *cmd = "./presblock";
			char *arguments[3];
			arguments[0] = "./presblock";
			strcpy(arguments[1], stringPID);
			arguments[2] = NULL;

			
			//initiaiting the program that generates the signals and passing to it the pid of the first child of
			//the shell.
			if(execvp(cmd, arguments) == -1)
			{
				errorPrint(errno);
				        		
				exit(1);
			}
			
        	
        }
        
        
        
        exit(0);
    }else
    {
    	//error forking
    	errorPrint(errno);
    }

    return 0;
}

WINDOW * createNewWindow(int h, int w, int startY, int startX)
{
    WINDOW * localWindow;


    localWindow = newwin(h, w, startY, startX); //actually creating the window
    box(localWindow, 0, 0); //this creates a box, bordered by the default character (beacuase of the 0).
    //we could have passed another character in the second and third parameters, for the border to be made out of it

    wrefresh(localWindow); //refreshes the window, and shows the box

    return localWindow;
}

void getLine(int * bufPointer1, int * bufPointer2)
{
	line[0] = '\0';
	int pos = 0;
	char ch;
	
	char col[20];
	char row[20];
	
	promptCol = 1;
	promptCol += strlen(getenv("prompt"))+2; //because of the '> '
	
	bufPointer1[0] = promptCol;
	bufPointer1[1] = promptCounter;
	bufPointer2[0] = promptCol;
	bufPointer2[1] = promptCounter;
	
	//until the character entered is the newline character
	while((ch = (char) mvwgetch(promptWin, promptCounter, promptCol)) != 10)
	{	
		//if the user presses the backspace AND he has not deleted everything (except the 'prompt> ') 
		if((ch == 127) && (promptCol > (strlen(getenv("prompt"))+3)))
		{
			mvwprintw(promptWin, promptCounter, promptCol, " ");
			mvwprintw(promptWin, promptCounter, promptCol-1, " ");
			mvwprintw(promptWin, promptCounter, promptCol+1, " ");
			promptCol--;
			wrefresh(promptWin);
			line[pos-1] = '\0';
			pos--;
		}else
		{
			line[pos] = ch;
			promptCol++;
			pos++;
		}
		

		bufPointer1[0] = promptCol;
		bufPointer1[1] = promptCounter;		
		bufPointer2[0] = promptCol;
		bufPointer2[1] = promptCounter;
		
		
	}
	
	promptCol = 1; //beacuase of the border
	line[pos] = '\0'; //if the user stops the input, the current pos will be set as the end of the line
	promptCounter++;
	
	//bufPointer is a pointer to the shared memory segment. 
	//Here we are writing the columna and row where we currently are, so if the time happens to be 
	//refreshing now, the cursor could be put back at its place by the other process, by reading this
	//shared memory segment. 
	bufPointer1[0] = promptCol; 
	bufPointer1[1] = promptCounter;	
	bufPointer2[0] = promptCol;
	bufPointer2[1] = promptCounter;
}


char ** splitLine(char * line)
{
    int bufsize = NUM_OF_TOKENS;
    int pos = 0;

    //2d array of strings
    char **tokens = malloc(bufsize * sizeof(char*));

    //temp storage to hold each token
    char * token;

    if(!tokens)
    {
        errorPrint(errno);
        exit(1);
    }

    //getting the first token
    token = strtok(line, DELIMITERS);

    //when there's no more tokens, strtok will return NULL, hence the while decision
    while(token != NULL)
    {
        tokens[pos] = token;
        pos++;

        if(pos >= bufsize)
        {
            bufsize += NUM_OF_TOKENS;

            tokens = realloc(tokens, bufsize* sizeof(char*));

            if(!tokens)
            {
                errorPrint(errno);
                exit(1);
            }
        }

        token = strtok(NULL, DELIMITERS);
    }

    tokens[pos] = NULL; //ending the 2d array
    return tokens;

}

//This function is used to launch any commands that are already implemented in the ubuntu shell, but not in the
//shell we're creating. This is done by forking a child and calling execvp to run that command.
int launch(char ** args)
{

    //The technique used to create the pipes is obtained from the following link:
    // http://www.microhowto.info/howto/capture_the_output_of_a_child_process_in_c.html

    //1. Create a new pipe using the pipe function.
    int filedes[2];
    if (pipe(filedes) == -1) {
        perror("pipe");
        exit(1);
    }

    //normal variables needed to do the forking
    pid_t pid;
    pid_t  wpid;
    int status; //status information will be stored in it by the waitpid()

    pid = fork();

    //child process
    if(pid == 0)
    {
        //2. Connect the entrance of the pipe to STDOUT_FILENO within the child process.
        //Standard output is routed into the pipe
        while ((dup2(filedes[1], STDOUT_FILENO) == -1) && (errno == EINTR)) {}
        close(filedes[1]);
        close(filedes[0]);


        //the first argument in the list args is actually the name of the command to be executed.
        //execvp expects a program name and an array of string arguments.
        //If execvp returns -1, or if it returns at all, it means that there was an
        //erro because execvp should dump the current process and load another one,
        //so it can't be that it returns.
        if(execvp(args[0], args) == -1)
        {
        	writeToBuffer("Error");
			printBuffer(outputCounter, 1);
            //errorPrint(errno); //this was giving gibberish output
            
        }
        
        _exit(1); //if execvp returns, then we need to exit, there's nothing else to do.

    }else if(pid < 0) //error forking
    {
        errorPrint(errno);
    }else
    {
        //Parent process.
        //We need that the child process is going to execute, and we
        //need to wait for it.


        close(filedes[1]);

        char buffer1[4096];
        while (1) {
            ssize_t count = read(filedes[0], buffer1, sizeof(buffer1));
            if (count == -1) {
                if (errno == EINTR) {
                    continue;
                } else {
                    errorPrint(errno);
                    exit(1);
                }
            } else if (count == 0) {
                break;
            } else {

                //the following while loop and the line where the null character is being assigned,
                //is done because the fprintf with the strerror(errno) was producing gibberish output
                //after the actual error description. So we just found out where the newline char is, and set
                //it to the null character, so from that point onwards nothing else is printed out.
                int i = 0;

                while(buffer1[i] != '\n')
                {
                    i++;
                }

                buffer1[i] = '\0';

                if(outputCounter >= outputH-1)
                {
                    box(outputWin, 0, 0); //this creates a box, bordered by the default character (beacuase of the 0).
                }

				writeToBuffer("");//the function is called because the function automatically adds the newline character at
				//the end of the line. We had redirected the output from going to stdout, to going to into a declared 
				//buffer, so then we can use it how we want. But execvp does not add a new line char at the end, so if we do
				//not add it manually, the lines after it won't be printed. 
				printOutputW(buffer1);
				
                
            }
        }
        close(filedes[0]);

        //we wait for the state of the child process to change
        do {
            //The macros used in the waitpid() is to wait until either
            //the child process is killed or exited.
            wpid = waitpid(pid, &status, WUNTRACED);

        }while(!WIFEXITED(status) && !WIFSIGNALED(status));
        //wifecited means that it exited normally, and wifsignaled means that
        //it was killed by a signal.

    }

    //this is used as a signal to the calling function that we should
    //prompt for input again.
    return 1;
}

int sh_cd(char ** args)
{
    //if no argument (path) was passed, then it wont work
    if(args[1] == NULL)
    {
        if(outputCounter >= outputH-1)
        {
            box(outputWin, 0, 0); //this creates a box, bordered by the default character (beacuase of the 0).
        }

		writeToBuffer("Expected argument to 'chdir'");
		printBuffer(outputCounter, 1);
    }else
    {
        //else call chdir(). On success it should return 0.
        if(chdir(args[1]) != 0) //chdir returns 0 on success.
        {
            errorPrint(errno);
        }
    }

    return 1;
}

int sh_exit(char ** args)
{
    return 0; //it returns 0 as a signal for the command loop to terminate
}

int shdir(char ** args)
{
    long size;
    char *buf;
    char *ptr;


    size = pathconf(".", _PC_PATH_MAX);


    if ((buf = (char *)malloc((size_t)size)) != NULL)
        ptr = getcwd(buf, (size_t)size);
       
    if(args[1] != NULL)    
    {
		if(strcmp(args[1], ">") == 0)
		{
			//output redirection
			
			FILE * fp;
			
			if(args[2] != NULL)
			{
				if((fp = fopen(args[2], "w")) != NULL)
				{
					int write = fputs(buf, fp);
					
					if(write < 0)
					{					
						writeToBuffer("Error in writing to file");
						printBuffer(outputCounter, 1);
					}
					
					fclose(fp);
				}else
				{					
					writeToBuffer("Error opening file");
					printBuffer(outputCounter, 1);
				}
			}else
			{
				writeToBuffer("Missing argument");
				printBuffer(outputCounter, 1);
			}
    	}
    }else 
    {
    	writeToBuffer(buf);
		printBuffer(outputCounter, 1);
    }

    free(buf);

    return 1;
}

int print(char ** args)
{
	FILE *fp;
	char string[200];
	bool redirection = false;

	//if there is something after the 'print' command
    if(args[1] != NULL)
    {
		int i = 1;

		//loops through all the args
        while(args[i] != NULL)
        {
        	//if the current arg is not the '>', concatenate it with the string, which we'll output finally
        	if(strcmp(args[i], ">") != 0)
        	{
		    	if(i == 1)
		    		strcpy(string, args[i]);
		    	else
		    	{
		    		strcat(string, " ");
		    		strcat(string, args[i]);
		    	}
        	}else //if the current arg is '>', check whether the next arg is NULL or not
        	{
        		//this will tell us that redirection was intended by the user
        		redirection = true;
        	
        		//if the arg after the '>' is not null, then try to open the file with that file name or create new one
        		if(args[i+1] != NULL)
        		{
        			if((fp = fopen(args[i+1], "w")) != NULL)
					{
						int write = fputs(string, fp);
					
						//fputs returns negative on error
						if(write < 0)
						{
							writeToBuffer("Error writing to file");
							printBuffer(outputCounter, 1);
						}
					
						fclose(fp);
					}else
					{
						writeToBuffer("Error opening file");
						printBuffer(outputCounter, 1);
					}
        		}
        		
        		//break out of the loop, no need to continue looping through the other args
        		break;
        	}
        	
        	i++;
        }
        
        if(!redirection)
        {
        	writeToBuffer(string);
			printBuffer(outputCounter, 1);
		}
	}

    return 1;
}

int set(char ** args)
{
    int pos = 0;
    int bufsize = 2;
    int tokenCount = 0;

    //2d array of strings
    char **tokens = malloc(bufsize * sizeof(char*));

    //temp storage to hold each token
    char * token;


    //if no argument (path) was passed, then it wont work
    if(args[1] == NULL)
    {
        if (outputCounter >= outputH - 1) {
            box(outputWin, 0, 0); //this creates a box, bordered by the default character (beacuase of the 0).
        }

		writeToBuffer("Expected 'set' command is \"set variable=value\"");
		printBuffer(outputCounter, 1);
		
    }else
    {
        if(!tokens)
        {
            errorPrint(errno);
            exit(1);
        }

        //getting the first token
        token = strtok(args[1], "=");

        //when there's no more tokens, strtok will return NULL, hence the while decision
        while(token != NULL)
        {
        	tokenCount++; //increments with every token
            tokens[pos] = token;
            pos++;

			//if for any reason, there is more arguments than supposed to, they need to be stored somewhere, 
			//so we allocate more memory to the tokens array.
            if(pos > bufsize)
            {
                bufsize += 2;

                tokens = realloc(tokens, bufsize* sizeof(char*));

                if(!tokens)
                {
                    errorPrint(errno);
                    exit(1);
                }
            }

            token = strtok(NULL, "=");
        }

		//if the number of arguments was more than supposed to
        if(tokenCount != 2)
        {
        	writeToBuffer("Expected 'set' command is \"set variable=value\"");
			printBuffer(outputCounter, 1);
			
        }else
        {
        	if(strcmp(tokens[0], "prompt") == 0)
        	{
        		setenv(tokens[0], tokens[1], 1); //the last parameter acts as a flag to decide whether the variable
        		//should be overwritten if it already exists, or left intact. 0 - leave the same, nonzero - update/overwrite
        		        		
        	}else if(strcmp(tokens[0], "path") == 0)
        	{
        		setenv(tokens[0], tokens[1], 1); //the last parameter acts as a flag to decide whether the variable        		
        		//should be overwritten if it already exists, or left intact. 0 - leave the same, nonzero - update/overwrite
        		
        	}else if(strcmp(tokens[0], "refresh") == 0)
        	{
        		if(atoi(tokens[1]) == 0)
        		{
        			writeToBuffer("refresh should be an integer larger than 0");
					printBuffer(outputCounter, 1);
					
        		}else
        		{
        			//Although the variable should be an integer, since the function takes a string as an argument, 
        			//we first check if the token is an integer and if it is we still pass it as a string. 
        			setenv(tokens[0], tokens[1], 1);
        		}        		
			}else if(strcmp(tokens[0], "buffer") == 0)
			{
				//This only sets the capacity of the total buffer and not the amount of lines displayed in the output window.
				
				if(atoi(tokens[1]) == 0)
        		{
        			writeToBuffer("buffer should be an integer larger than 0");
					printBuffer(outputCounter, 1);
					        		
        		}else
        		{
        			//Although the variable should be an integer, since the function takes a string as an argument, 
        			//we first check if the token is an integer and if it is we still pass it as a string. 
        			setenv(tokens[0], tokens[1], 1);
    
					
        		} 
			}else
        	{
        		writeToBuffer("No such variable found");
				printBuffer(outputCounter, 1);	
				
			}
        }

    }

    return 1;
}

int printvar(char ** args)
{
	int count = 0;

	//first we check whether there is at least 1 argument, for example 'printvar prompt'
	if(args[1] != NULL)
    {
        int i = 1;

		//we count the number of arguments passed to 'printvar'
        while(args[i] != NULL)
        {
            count++;
            
            i++;
        }
        
        //if more than one argument has been passed, then we check for redirection
        if(count > 1)
        {
        	//if the redirection character has been passed, it should be as the second argument to 'printvar'
        	if(strcmp(args[2], ">") == 0)
        	{
        		//if there is more than 2 arguments, then it means that a file name was passed as argument, and we try to open it
        		if(count > 2)
        		{
        			FILE *fp;
        			
        			fp = fopen(args[3], "w");
        			
        			if(fp != NULL)
        			{
        				//check whether the variable passed exists or not
        				if(getenv(args[1]) != NULL)
        				{
		    				int write = fputs(getenv(args[1]), fp);
					
							//fputs returns negative on error
							if(write < 0)
							{
								writeToBuffer("Error writing to file");
								printBuffer(outputCounter, 1);
							}
							
						}else
						{
							writeToBuffer("No such variable");
							printBuffer(outputCounter, 1);
						}
					
						fclose(fp);
        			}else
        			{
        				writeToBuffer("Error opening file");
						printBuffer(outputCounter, 1);
        			}
        		}else
        		{
        			writeToBuffer("No file specified for output redirection");
					printBuffer(outputCounter, 1);
        		}
        	}else
        	{
        	}
        }else
        {   
		   	if(getenv(args[1]) != NULL)
		   	{
		   		writeToBuffer(getenv(args[1]));
				printBuffer(outputCounter, 1);
		    }else
		    {
		    	writeToBuffer("No such variable");
				printBuffer(outputCounter, 1);
		    }		            
        }
    }else
    {
    	writeToBuffer("printvar should at least have one argument");
		printBuffer(outputCounter, 1);
    }

	return 1;
}

int smove(char ** args)
{
	if(outputCounter <= outputH-2)
	{
		//We're reducing 2 from outputH because the border (the box) of the window takes space of 2 lines. 
		writeToBuffer("Not enough output to move trough");
		printBuffer(outputCounter, 1);
	}else
	{
		if(args[1] == NULL)
		{
			writeToBuffer("Argument expected");
			printBuffer(outputCounter, 1);
		}else
		{
			if(atoi(args[1]) == 0)
			{
				writeToBuffer("Nothing is moved");
				printBuffer(outputCounter, 1);
			}else
			{
				//lastline is initially set to -1
				if(lastLine == -1)
				{
				
					int parameter1 = outputCounter-(outputH-2)+1+atoi(args[1])-1;
					int parameter2 = outputH-2;
				
					printBuffer(parameter1, parameter2);
					for(int i = parameter1; i < abs(parameter1)+parameter2; i++)
					{
						outputCounter--;
					} //We're not actually outputting any info, we're just scrolling, so we need 
	   				  //to cancel out the increments made when calling printBuffer().
	   				  
	   				lastLine = outputCounter + atoi(args[1]);
   				}else
   				{
				
					int parameter1 = lastLine-(outputH-2)+1+atoi(args[1])-1;
					int parameter2 = outputH-2;
				
					printBuffer(parameter1, parameter2);
					for(int i = parameter1; i < abs(parameter1)+parameter2; i++)
					{
						outputCounter--;
					} //We're not actually outputting any info, we're just scrolling, so we need 
	   				  //to cancel out the increments made when calling printBuffer().
	   				  
	   				lastLine += atoi(args[1]);
   				}
   				
			}
		}
	}

	return 1;
}

int sh_num_builtins(void)
{
    return sizeof(builtinStr)/sizeof(char*);
}

int execute(char ** args)
{
    int i;

    //empty command entered
    if(args[0] == NULL)
    {
        return 1;
    }

    //compare the command entered with the name of each built in function
    for(i = 0; i < sh_num_builtins(); i++)
    {
        if(strcmp(args[0], builtinStr[i]) == 0)
        {
            return (*builtinFunc[i])(args);
        }
    }

    //if there was no matching built in command, then we call launch and execute
    //external command already designed in the linux shell
    return launch(args);
}

void errorPrint(int errorNum)
{
    //Follow the link: http://stackoverflow.com/questions/14715493/redirect-standard-error-to-a-string-in-c

    char *buf = malloc(sizeof(char*) * BUFSIZ);
    setbuf(stderr, buf); //this function sets the buffer for stderr to 'buf', so then we can output it as a normal string

    //perror("Shell"); //this will write to buf now, since stderr was amended with setbuf
    fprintf(stderr, "%s\n", strerror(errorNum)); //this is instead of using perror("Shell")
    //fprintf(stderr, "%s\n", strerror(errno)); //the same as the previous line

    //the following while loop and the line where the null character is being assigned,
    //is done because the fprintf with the strerror(errno) was producing gibberish output
    //after the actual error description. So we just found out where the newline char is, and set
    //it to the null character, so from that point onwards nothing else is printed out.
    int i = 0;

    while(buf[i] != '\n')
    {
        i++;
    }

    buf[i] = '\0';

	/*
    if(outputCounter >= outputH-1)
    {
        box(outputWin, 0, 0); //this creates a box, bordered by the default character (beacuase of the 0).
    }
    */

	//this will output the error that was supposed to be
    //output to stderr, but now on the output window.
   	writeToBuffer(buf);
	printBuffer(outputCounter, 1);
		
    free(buf);

}

int writeToBuffer(char * line)
{
	//IF THE NUMBER OF LINES WRITTEN TO THE BUFFER (bufferCount), IS EQUAL TO THE NUMBER OF LINES THE BUFFER HAS 
	//ALLOCATED TO IT, THEN WE CAN'T WRITE TO IT BECAUSE IT IS FULL. ELSE WE CAN. 
	if(bufferCount != atoi(getenv("buffer")))
	{
		strcpy(buffer[bufferCount], line);
		bufferCount++;
		
		return 1; //okay
		
	}else
	{
		printOutputW("Buffer is full, try resizing it using 'setvar' first");
    	
    	return 0; //error
	}
}

int printBuffer(int startLine, int lines)
{
	if(lines == 0)
		return 0; //nothing to print
	else
	{
		for(int i = abs(startLine); i < abs(startLine)+lines; i++)	
		{
			printOutputW(buffer[i]);
		}
		
		return 1;
	}
}

void printOutputW(char * line)
{
	wprintw(outputWin, "  %s\n", line);
	box(outputWin, 0, 0); //this creates a box, bordered by the default character (beacuase of the 0).
	outputCounter++;
	wrefresh(outputWin);
}

void sig_handler(int signo)
{	
	interruptRec = 1;    
}



